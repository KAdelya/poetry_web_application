from flask import Blueprint, render_template, request, redirect, url_for, flash
from flask_login import login_required, current_user
import json
from flask_wtf import FlaskForm
from sqlalchemy import func
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length

from models import Users, Strn, Queue
from wsgi import db

main = Blueprint('main', __name__)


class EnterStringForm(FlaskForm):
    string = StringField('string', validators=[DataRequired(message='Это поле не должно быть пустым'),
    Length(message='Строка должна состоять минимум из 5 русских букв или пробелов', min=5)])
    submit = SubmitField('Отправить')



@main.route('/')
def index():
    return render_template('index.html')


@main.route('/enter_string/', methods=['GET', 'POST'])
@login_required
def enter_string():
    form = EnterStringForm()
    if form.validate_on_submit():
        strn = Strn(string=str(form.string.data), user_id=current_user.id)
        db.session.add(strn)
        db.session.commit()
        with db.engine.connect() as conn:
            conn.execute(db.text("INSERT INTO queue (string_id) VALUES (:y)"), [{"y": str(form.string.data)}])
        return redirect(url_for('main.your_verse'))
    return render_template('enter_string.html', name=current_user.name, form=form)


@main.route('/profile/', methods=['GET', 'POST'])
@login_required
def current_user_profile():
    if request.method == 'POST':
        inf = request.form.get('text')
        current_user.info = inf
        db.session.commit()
        return redirect(url_for('main.current_user_profile'))
    elif current_user.info is None:
        current_user.info = 'Не заполнена'
        db.session.commit()
    return render_template('profile.html', name=current_user)


@main.route('/profile/<int:user_id>')
@login_required
def profile(user_id: int):
    user = Users.query.filter_by(id=user_id).first()
    return render_template('profile.html', name=user)


@main.route('/your_verse/')
@login_required
def your_verse():
    queues = Queue.query.order_by(func.random()).limit(7).all()
    final_queue = []
    for i in queues:
        final_queue.append(i.string_id)
    final_queue.append(current_user.strings[-1].string)
    return render_template('your_verse.html', queue=final_queue)


@main.route('/current_users_poets/')
@login_required
def current_users_poets():
    return render_template('current_users_poets.html', users=Users.query.all())


@main.route('/search', methods=['GET', 'POST'])
@login_required
def search():
    dictionary = {}
    users = Users.query.all()
    for i in users:
        dictionary[i.name] = i.id
    if request.method == 'POST':
        search1 = request.form.get('search')
        user = Users.query.filter_by(name=search1).first()
        if user:
            return redirect(url_for('main.profile', user_id=user.id))
        else:
            flash('Такого пользователя не существует')
            return redirect(url_for('main.search'))
    return render_template('search.html', dictionary=json.dumps(dictionary))


@main.route('/search/', methods=['GET', 'POST'])
@login_required
def search_string():
    if request.method == 'POST':
        st = request.form.get('search_string')
        strn = Strn.query.filter_by(string=st).first()
        if strn:
            return redirect(url_for('main.profile', user_id=strn.user_id))
        else:
            flash('Такой строчки не существует')
            return redirect(url_for('main.search'))
    return render_template('search.html', name=current_user.id)
