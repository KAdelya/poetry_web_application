import pytest
from flask import url_for
from werkzeug.security import generate_password_hash

from models import Users
from wsgi import create_app, db

app_module = create_app()

@pytest.fixture
def app():
    app_module.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app_module.config['WTF_CSRF_ENABLED'] = False
    with app_module.app_context():
        db.create_all()
    yield app_module
    with app_module.app_context():
        db.session.remove()
        db.drop_all()

@pytest.fixture
def register_in_client(client):
    user = Users(email='rus@gmail.com', name='Rustem', password=generate_password_hash('123456', method='sha256'))
    db.session.add(user)
    db.session.commit()
    client.post(url_for('auth.signup_post'), data=dict(email='rus@gmail.com', password='123456'), follow_redirects=True)
    yield client
    client.get(url_for('auth.signup_post'))


@pytest.fixture
def logged_in_client(register_in_client):
    register_in_client.post(url_for('auth.login_post'), data=dict(email='rus@gmail.com', password='123456'), follow_redirects=True)
    yield register_in_client
    register_in_client.get(url_for('auth.logout'))
