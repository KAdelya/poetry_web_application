from flask_login import UserMixin

from wsgi import db


class Users(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    strings = db.relationship('Strn', lazy='select', backref=db.backref('users', lazy='joined'))
    info = db.Column(db.Text())

    def __repr__(self):
        return f'{self.email} - {self.name} - {self.strings}'


class Strn(UserMixin, db.Model):
    string = db.Column(db.String(100), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    queues = db.relationship('Queue', lazy='select', backref=db.backref('strn', lazy='joined'))

    def __repr__(self):
        return f'{self.string}, {self.user_id}'


class Queue(UserMixin, db.Model):
    queue = db.Column(db.Integer, primary_key=True)
    string_id = db.Column(db.String(100), db.ForeignKey('strn.string'))

    def __repr__(self):
        return f'{self.queue}, {self.string_id}'
