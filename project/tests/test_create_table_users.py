from flask import url_for


def test_users_page(logged_in_client):
    res = logged_in_client.get(url_for('main.current_users_poets'))
    assert res.status_code == 200
    assert bytes('Текущие пользователи-стихотворцы:', 'utf-8') in res.data
    assert bytes('Данные пользователи участвуют в создании стихотворений!', 'utf-8') in res.data
