from flask import url_for


def test_login_page(client):
    res = client.get(url_for('auth.login_post'))
    assert res.status_code == 200
    assert bytes("Пароль должен содержать цифры или латинские буквы.", 'utf-8') in res.data
