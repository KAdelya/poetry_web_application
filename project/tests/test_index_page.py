from flask import url_for


def test_index_page(client):
    res = client.get(url_for('main.index'))
    assert res.status_code == 200
    assert bytes("Главная", 'utf-8') in res.data
    assert bytes("Добро пожаловать в процесс создания стихов!", 'utf-8') in res.data
