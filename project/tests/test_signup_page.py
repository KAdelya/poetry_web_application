from flask import url_for


def test_signup_page(client):
    res = client.get(url_for('auth.signup_post'))
    assert res.status_code == 200
    assert bytes("Регистрация", 'utf-8') in res.data
    assert bytes("Пароль должен содержать цифры или латинские буквы A-Z, a-z.", 'utf-8') in res.data
    assert bytes("Имя должно содержать только латинские буквы A-Z, a-z.", 'utf-8') in res.data
