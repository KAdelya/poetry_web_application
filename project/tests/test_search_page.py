from flask import url_for


def test_search_page_search(logged_in_client):
    res = logged_in_client.get(url_for('main.search'))
    assert res.status_code == 200
    assert bytes("Введите строчку, автора которого хотите найти.", 'utf-8') in res.data
    assert bytes("Заполните мини-анкету. Данные, переданные ей, будут присутствовать в Вашем профиле!", 'utf-8') in res.data
    assert bytes("Текст должен содержать минимум 5 русских букв или пробелов.", 'utf-8') in res.data
