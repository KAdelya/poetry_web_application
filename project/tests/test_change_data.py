from flask import url_for


def test_change_data_page(logged_in_client):
    res = logged_in_client.get(url_for('auth.change_data'))
    assert res.status_code == 200
    assert bytes('Изменение данных', 'utf-8') in res.data
    assert bytes('Имя должно содержать только латинские буквы A-Z, a-z.', 'utf-8') in res.data
    assert bytes('Пароль должен содержать цифры или латинские буквы.', 'utf-8') in res.data
