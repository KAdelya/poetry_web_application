import pytest
from flask import url_for
from werkzeug.security import check_password_hash
from models import Users, Strn, Queue


def test_current_user_info_for_logged_in_should_be_ok_on_index(logged_in_client):
    res = logged_in_client.get(url_for('main.index'))
    assert res.status_code == 200


def test_current_user_info_for_logged_out_should_be_not_ok(register_in_client):
    res = register_in_client.get(url_for('auth.logout'))
    assert res.status_code == 302


def test_text_current_user_info(logged_in_client):
    res = logged_in_client.get(url_for('main.current_user_profile'))
    assert res.status_code == 200
    assert bytes('Имя:', 'utf-8') in res.data
    assert bytes('Rustem', 'utf-8') in res.data
    assert bytes('Электронная почта:', 'utf-8') in res.data


def test_text_not_current_user_info(client):
    res = client.get(url_for('main.current_user_profile'))
    assert res.status_code == 302


def test_text_enter_string(logged_in_client):
    res = logged_in_client.get(url_for('main.enter_string'))
    assert res.status_code == 200
    assert bytes('Добро пожаловать, Rustem!', 'utf-8') in res.data
    assert bytes('Введите свою строчку для получения полноценного стиха!', 'utf-8') in res.data


@pytest.mark.parametrize('name', [
    'два-три',
    'ну и так',
    'давай'
])
def test_enter_string_post(logged_in_client, name):
    res = logged_in_client.post(url_for('main.enter_string'), data=dict(string=name), follow_redirects=True)
    res_2 = logged_in_client.get(url_for('main.your_verse'), follow_redirects=True)
    assert res == 200
    assert res_2 == 200
    assert Strn.query.filter_by(string=name).count() == 1
    assert Queue.query.filter_by(string_id=name).count() == 1
    assert Strn.query.filter_by(string=name).first().string == name
    assert Queue.query.filter_by(string_id=name).first().string_id == name
    assert bytes(name, 'utf-8') in res_2.data


@pytest.mark.parametrize('email, name_before, new_name, password_before, password', [
    ('rus@gmail.com',
     'Rustem',
     'Ruslan',
     '123456',
     '000000')
])
def test_change_data_page(logged_in_client, email, name_before, new_name, password_before, password):
    res = logged_in_client.post(url_for('auth.change_data'), data=dict(email=email, name_before=name_before,
                                                                       new_name=new_name,
                                                                       password_before=password_before,
                                                                       password=password), follow_redirects=True)
    user = Users.query.filter_by(email=email).first()
    assert res == 200
    assert check_password_hash(user.password, password) is True
    assert user.name == new_name


@pytest.mark.parametrize('email, name, password', [
    ('rus@gmail.com',
     'Rustem',
     '123456')
])
def test_current_user_info_for_signup_in_should_be_ok(register_in_client, email, name, password):
    res = register_in_client.post(url_for('auth.signup_post'), data=dict(email=email, name=name, password=password),
                                  follow_redirects=True)
    user = Users.query.filter_by(email=email).first()
    assert res == 200
    assert user.email == email
    assert user.name == name


@pytest.mark.parametrize('string', [
    ('приветики'),
    ('эта осень'),
    ('золотая')
])
def test_search_string(logged_in_client, string):
    res = logged_in_client.post(url_for('main.search_string'), data=dict(string=string), follow_redirects=True)
    assert res == 200


@pytest.mark.parametrize('email', [
    ('rus@gmail.com')
])
def test_current_user_info(logged_in_client, email):
    res = logged_in_client.get(url_for('main.current_user_profile'), follow_redirects=True)
    user_info = Users.query.filter_by(email=email).first().info
    assert res == 200
    assert user_info == 'Не заполнена'
