from flask import Blueprint, render_template, redirect, url_for, flash
from flask_wtf import FlaskForm
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required, current_user
from wtforms import BooleanField, StringField, SubmitField
from wtforms import PasswordField
from wtforms.validators import DataRequired, Length, Email

from models import Users
from wsgi import db

auth = Blueprint('auth', __name__)


class RegistrationForm(FlaskForm):
    email = StringField('email', validators=[DataRequired(message='Это поле не должно быть пустым'),
                                             Email(message='Введите правильный электронный адрес')])
    name = StringField('name', validators=[DataRequired(message='Это поле не должно быть пустым'),
                                           Length(message='В имени должно быть минимум 4 и максимум 25 символов', min=4,
                                                  max=25)])
    password = PasswordField('password', validators=[DataRequired(message='Это поле не должно быть пустым'),
                                                     Length(
                                                         message='Пароль должен состоять минимум из 5 и максимум'
                                                                 ' из 10 цифр или латинских букв',
                                                         min=5, max=10)])
    submit = SubmitField('Регистрация')


class LoginForm(FlaskForm):
    email = StringField('email', validators=[DataRequired(message='Это поле не должно быть пустым'),
                                             Email(message='Введите правильный электронный адрес')])
    password = PasswordField('password', validators=[DataRequired(message='Это поле не должно быть пустым'),
                Length(message='Пароль должен состоять минимум из 5 и максимум из 10 цифр или латинских букв',
                       min=5, max=10)])
    remember = BooleanField(label=('Запомнить меня'))
    submit = SubmitField('Вход')


class ChangeForm(FlaskForm):
    email = StringField('email', validators=[DataRequired(message='Это поле не должно быть пустым'),
                                             Email(message='Введите правильный электронный адрес')])
    name_before = StringField('name_before', validators=[DataRequired(message='Это поле не должно быть пустым'),
                                               Length(message='В имени должно быть минимум 4 и максимум 25 символов',
                                                      min=4,
                                                      max=25)])
    new_name = StringField('new_name', validators=[DataRequired(message='Это поле не должно быть пустым'),
                                           Length(message='В имени должно быть минимум 4 и максимум 25 символов', min=4,
                                                  max=25)])
    password_before = PasswordField('password_before',
                                    validators=[DataRequired(message='Это поле не должно быть пустым'),
                                                Length(
                                                    message='Пароль должен состоять минимум из 5 и максимум'
                                                            ' из 10 цифр или латинских букв',
                                                    min=5, max=10)])
    password = PasswordField('password', validators=[DataRequired(message='Это поле не должно быть пустым'),
                                                     Length(
                                                         message='Пароль должен состоять минимум из 5 и максимум'
                                                                 ' из 10 цифр или латинских букв',
                                                         min=5, max=10)])
    submit = SubmitField('Изменить')



@auth.route('/login/', methods=['GET', 'POST'])
def login_post():
    form = LoginForm()
    if form.validate_on_submit():
        user_email = Users.query.filter_by(email=form.email.data).first()
        if not user_email or not check_password_hash(user_email.password, form.password.data):
            flash('Пожалуйста введите свои данные заново')
            return redirect(url_for('auth.login_post'))
        login_user(user_email, remember=form.remember.data)
        return redirect(url_for('main.current_user_profile'))
    return render_template('login.html', form=form)


@auth.route('/signup', methods=['GET', 'POST'])
def signup_post():
    form = RegistrationForm()
    if form.validate_on_submit():
        user_email = Users.query.filter_by(email=form.email.data).first()
        if user_email:
            flash('Этот электронный адрес уже использовался')
            return redirect(url_for('auth.signup_post'))
        user = Users(email=str(form.email.data), name=str(form.name.data),
                     password=generate_password_hash(str(form.password.data), method='sha256'))
        user.info = 'Не заполнена'
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('auth.login_post'))
    return render_template('signup.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@auth.route('/change_data', methods=['GET', 'POST'])
def change_data():
    form = ChangeForm()
    if form.validate_on_submit():
        user = Users.query.filter_by(email=form.email.data).first()
        if user.email == current_user.email and str(form.name_before.data) == current_user.name\
                and check_password_hash(user.password, str(form.password_before.data)):
            user.name = str(form.new_name.data)
            db.session.commit()
            user.password = generate_password_hash(str(form.password.data), method='sha256')
            db.session.commit()
            return redirect(url_for('auth.logout'))
        flash('Введите свои данные')
        return redirect(url_for('auth.change_data'))
    return render_template('change_data.html', form=form)
